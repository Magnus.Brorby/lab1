package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    String[] weapons = {"rock", "paper", "scissors"};
    int point;
    String winner;
    String retry;

    public String ai_choice() {
        Random rand = new Random();
        int randnum = rand.nextInt(3);
        String choice = weapons[randnum];
        return choice;
    }

    public String human_choice() {
        System.out.println("Let's play round " + roundCounter + "\nYour choice (Rock/Paper/Scissors)?");
        String choice = sc.nextLine();
        choice = choice.toLowerCase();
        return choice;
    }

    
    public void run() {
        while (true) {
            String ai = ai_choice();
            String human = human_choice();
            String text = "Human chose " + human + ", computer chose " + ai + ".";
            if (human.equals("rock")) {
                if (ai.equals("rock")) {
                    point = 0;
                }
                else if (ai.equals("paper")) {
                    point = 1;
                }
                else {
                    point = 2;
                } 
            }
            else if (human.equals("paper")) {
                if (ai.equals("rock")) {
                    point = 2;
                }
                else if (ai.equals("paper")) {
                    point = 0;
                }
                else {
                    point = 1;
                }
            }
            else if (human.equals("scissors")){
                if (ai.equals("rock")) {
                    point = 1;
                }
                else if (ai.equals("paper")) {
                    point = 2;
                }
                else {
                    point = 0;
                }
            }
            
            if (point == 0) {
                winner = " It's a tie!";
            }
            else if (point == 1) {
                winner = " Computer wins!";
                computerScore += 1;
            }
            else {
                winner = " You win!";
                humanScore += 1;
            }
            System.out.println(text + winner + "\nScore: human " + humanScore + ", computer " + computerScore);
            System.out.println("Do you wish to continue playing? (y/n)?");
            retry = sc.nextLine();
            retry = retry.toLowerCase();
            roundCounter += 1;
            if (retry.equals("y") || retry.equals("yes")) {
                continue;
            }
            else {
                System.out.println("Bye bye :)");
                break;
            }

        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
